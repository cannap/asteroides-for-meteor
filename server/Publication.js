// ------------------------------ ALL MAIN Categories ------------------------------ //

Meteor.publish('categories', function () {

    //Publish SubCategories to
    return [
        Categories.find({}, {sort: {name: 1}}),
        SubCategories.find({}, {sort: {name: 1}}),
    ];
});


// ------------------------------ Package List ------------------------------ //
Meteor.publish('packages', function (slug, options) {
    var packages, category;
    //Get the Right ID
    category = Categories.find({slug: slug}, {limit: 1});

    if (category.count() === 0) {
        category = SubCategories.find({slug: slug}, {limit: 1});
    } else {



    }




    //Todo: if category false check in the subcategories
    var categoryfetch = category.fetch();



    packages = Packages.find(
        {
            'categories': categoryfetch[0]._id
        }, options);

    return [
        //For the Category Information
        category,
        packages
    ]

});

// ------------------------------ Single Package ------------------------------ //

Meteor.publish('singlePackage', function (slug) {

    var singlePackage = Packages.find({slug: slug}, {limit: 1});
    var authorName = singlePackage.fetch()[0].details.author;
    var packagesByAuthor = Packages.find({'details.author': authorName})
    return [
        //singlePackage,
        packagesByAuthor
    ]

});

// ------------------------------ Packages by Author name ------------------------------ //

Meteor.publish('packagesByAuthor', function (authorName) {
    return Packages.find({'details.author': authorName})

});


// ------------------------------ Favorites ------------------------------ //
Meteor.publish('favorites', function () {
    return Favorites.find();
});

Meteor.publish('favoritesPackages', function () {
    return Packages.find();
});


// ------------------------------ Footer ------------------------------ //
Meteor.publish('footerPackageCount', function () {
    return BigCounters.find({name: 'packageCounter'}, {limit: 1});
});

// ------------------------------ Flash Messages ------------------------------ //
Meteor.publish('flashMessages', function () {
    return FlashMessages.find();
});
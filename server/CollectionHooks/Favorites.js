Favorites.after.insert(function (userId, doc) {

    Packages.update({_id: doc.packageId},
        {
            $inc: {
                'counts.favCount': 1
            }
        });
});

Favorites.after.remove(function (userId, doc) {

    Packages.update({_id: doc.packageId},
        {
            $inc: {
                'counts.favCount': -1
            }
        });
});

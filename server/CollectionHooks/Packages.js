

// ------------------------------ Increase Category Counter and global Package Counter ------------------------------ //


Packages.after.insert(function (userId, doc) {

    //Todo: Determine if is Subcategory or Category

    Categories.update(doc.categories[0], {
        $inc: {packageCount: 1}
    });

    if (BigCounters.find().count() === 0) {
        BigCounters.insert(
            {name: 'packageCounter', 'count': 1}
        );
    } else {
        BigCounters.update({name: 'packageCounter'}, {$inc: {count: 1}});
    }

});


// ------------------------------ decrease Category Counter and global Package Counter ------------------------------ //
Packages.after.remove(function (userId, doc) {
    Categories.update(doc.category.id, {
        $inc: {packageCount: -1}
    });

    //Remove Favorite for the removed Package
    Favorites.remove({packageId: doc._id});
    BigCounters.update({name: 'packageCounter'}, {$inc: {count: -1}});
});

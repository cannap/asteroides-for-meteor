var result;
var catID;


packageHandler = {};


packageHandler.getPackageInformations = function (packageName, catID, update) {
    this.catID = catID;
    if (!update) {
        if (Packages.findOne({name: packageName})) {
            throw new Meteor.Error("A package with this name already exists");
        }
    }

    try {
        var result = HTTP.call('get', 'https://atmospherejs.com/a/packages/findByNames?names=' + packageName,
            {
                headers: {
                    "Accept": "application/json"
                }
            });
    } catch (e) {
        throw new Meteor.Error("Atmosphere is unavailable ( " + e.message + ")");
    }

    this.result = result;

    return this.buildPackage(update);

};


packageHandler.buildPackage = function (update) {

//Package not found
    if (!this.result.data[0]) {
        throw new Meteor.Error('Package not found')
    }

//Try to grab the Readme
    try {
        var readme = HTTP.call('get', this.result.data[0].latestVersion.readme);
    } catch (e) {
        throw new Meteor.Error('Readme error', e.message)
    }

    var category = Categories.findOne({_id: this.catID});

    var purename = this.result.data[0].name.split(':');


    if (!update) {
        //Not a Update
        return {
            name: this.result.data[0].name,
            pureName: purename[1],
            completeName: this.result.data[0].name,
            description: this.result.data[0].latestVersion.description,
            updatedAt: new Date(),
            createdAt: new Date(),
            slug: slugify(this.result.data[0].name.replace(':', '-')),


            //Todo: if Update dont reset
            flagged: false,
            counts: {
                favCount: 0
            },


            //Todo: Remove this is a bullshit idea whats happens when i update the damn Category beter with Relations
            /*categories: {
                id: category._id,
            },*/

            categories: [
                category._id,
            ],

            details: {
                author: purename[0],
                //Todo: sanitize Git link
                git: this.result.data[0].latestVersion.git,
                version: this.result.data[0].latestVersion.version,
                readme: marked(readme.content)
            }
        };
        //Update
    } else {

        return {
            name: this.result.data[0].name,
            pureName: purename[1],
            completeName: this.result.data[0].name,
            description: this.result.data[0].latestVersion.description,
            updatedAt: new Date(),
            //Todo: Change this to Published date from json to keep new updates on top

            slug: slugify(this.result.data[0].name.replace(':', '-')),

          /*  category: {
                id: category._id,
            },*/

            details: {
                author: purename[0],
                //Todo: sanitize Git link
                git: this.result.data[0].latestVersion.git,
                version: this.result.data[0].latestVersion.version,
                readme: marked(readme.content)
            }
        };

    }

};


packageHandler.updateAllPackages = function () {





};



packageHandler.updateAllPackages();

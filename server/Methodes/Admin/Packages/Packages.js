Meteor.methods({

    //Todo: Add a Custom Fixture loader
    addPackage: function (doc) {
       // check(doc, Schema.Packages);
        var packageDoc = packageHandler.getPackageInformations(doc.name, doc.category, false);
        Packages.insert(packageDoc);
    },

    updatePackage: function (id, name, cat) {
        check(id, String);
        check(name, String);
        check(cat, String);

        var packageDoc = packageHandler.getPackageInformations(name, cat, true);
        var id = Packages.update({_id: id}, {$set: packageDoc});
        return id;
    },

    removePackage: function (packageID) {
        check(packageID, String);
        //Todo: Change this to permit
        if (allow.Admin()) {
            Packages.remove(packageID);
        }
    }
});
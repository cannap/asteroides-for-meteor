SearchSource.defineSource('packages', function (searchText, options) {
//Todo make things better
    if (searchText) {
        var regExp = buildRegExp(searchText);
        var selector = {
            $or: [
                {name: regExp},
                {description: regExp}
            ]
        };


        return Packages.find(selector, {limit: 20}).fetch();
    } else {
        return Packages.find({}, {limit: 20}).fetch();
    }
});

function buildRegExp(searchText) {
    // this is a dumb implementation
    var parts = searchText.trim().split(/[ \-\:]+/);
    return new RegExp("(" + parts.join('|') + ")", "ig");
}
Meteor.methods({


    'removeCollections': function () {


        if (Roles.userIsInRole(this.userId, ['admin'])) {
            Categories.remove({});
            BigCounters.remove({});
            Packages.remove({});
            Favorites.remove({});
        }
    }
    //

});
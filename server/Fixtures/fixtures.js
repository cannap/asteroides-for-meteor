fixtures = {};

fixtures.categories = [
    {
        name: 'Animation',
        slug: 'animation',
        description: 'Everything that has to do with movement and visual effects'
    },
    {name: 'Frontend',
        slug: 'frontend',
        description: 'CSS Frameworks / Icons / Buttons and many more'
    },
    {
        name: 'Collections',
        slug: 'collections',
        description: 'All about Helpers / Validation / Relationship / Manipulation'
    },
    {
        name: 'Debugging and Testing',
        slug: 'debugging-testing',
        description: 'Mocha / Jasmin / Frontend Testing and more'
    },
    {
        name: 'Utils and Helpers',
        slug: 'utils-and-helpers',
        description: 'Time / Money / Array / Manipulation / Random'
    },
    {
        name: 'Wrappers',
        slug: 'wrappers',
        description: 'jQuery Plugins and more wrapped for MeteorJS'
    },
    {
        name: 'SEO',
        slug: 'seo',
        description: 'Search engine optimization'
    },
    {
        name: 'Preprocessors',
        slug: 'preprocessors',
        description: 'Less / Sass / Coffee'
    },
    {
        name: 'Users',
        slug: 'users',
        description: 'Account management / oAuth Services'
    },
    {
        name: 'Routing',
        slug: 'routing',
        description: 'On the right way'
    },
    {
        name: 'i18n',
        slug: 'i18n',
        description: 'German? English?'
    },

    {
        name: 'database',
        slug: 'database',
        description: 'MYSQl / Mongo'
    },

    {
        name: 'Security',
        slug: 'security',
        description: 'Deny / Allow / Browser Policy'
    }

];

fixtures.packages = [
    // ------------------------------  Animation  ------------------------------ //
    {name: 'percolate:momentum', 'slug': 'animation'},
    {name: 'ccorcos:animate', 'slug': 'animation'},
    {name: 'appmill:animation-hooks', 'slug': 'animation'},


    // ------------------------------  Debug ------------------------------ //
    {name: 'xolvio:cucumber', 'slug': 'debugging-and-testing'},
    {name: 'velocity:html-reporter', 'slug': 'debugging-and-testing'},
    {name: 'mike:mocha', 'slug': 'debugging-and-testing'},
    {name: 'msavin:mongol', 'slug': 'debugging-and-testing'},
    {name: 'sanjo:jasmine', 'slug': 'debugging-and-testing'},
    {name: 'xolvio:webdriver', 'slug': 'debugging-and-testing'},

    // ------------------------------ Frontend ------------------------------ //
    {name: 'fortawesome:fontawesome', 'slug': 'frontend'},
    {name: 'meteoric:ionicons-sass', 'slug': 'frontend'},
    {name: 'semantic:ui-css', 'slug': 'frontend'},
    {name: 'twbs:bootstrap', 'slug': 'frontend'},
    {name: 'mizzao:bootboxjs', 'slug': 'frontend'},
    {name: 'rajit:bootstrap3-datepicker', 'slug': 'frontend'},
    {name: 'babrahams:editable-text', 'slug': 'frontend'},
    {name: 'perak:markdown', 'slug': 'frontend'},
    {name: 'materialize:materialize', 'slug': 'frontend'},

    // ------------------------------ Collections ------------------------------ //
    {name: 'aldeed:collection2', 'slug': 'collections'},
    {name: 'aldeed:autoform', 'slug': 'collections'},
    {name: 'aldeed:simple-schema', 'slug': 'collections'},
    {name: 'dburles:collection-helpers', 'slug': 'collections'},
    {name: 'vazco:universe-collection', 'slug': 'collections'},

    // ------------------------------  Utils and Helpers  ------------------------------ //
    {name: 'random', 'slug': 'utils-and-helpers'},
    {name: 'underscore', 'slug': 'utils-and-helpers'},
    {name: 'stevezhu:lodash', 'slug': 'utils-and-helpers'},

    // ------------------------------ SEO ------------------------------ //
    {name: 'todda00:friendly-slugs', 'slug': 'seo'},
    {name: 'manuelschoebel:ms-seo', 'slug': 'seo'},
    {name: 'spiderable', 'slug': 'seo'},
    {name: 'gadicohen:sitemaps', 'slug': 'seo'},

    // ------------------------------ Precompressors ------------------------------ //
    {name: 'less', 'slug': 'preprocessors'},
    {name: 'fourseven:scss', 'slug': 'preprocessors'},
    {name: 'coffeescript', 'slug': 'preprocessors'},
    {name: 'stylus', 'slug': 'preprocessors'},

    // ------------------------------ users ------------------------------ //
    {name: 'accounts-password', 'slug': 'users'},
    {name: 'accounts-ui', 'slug': 'users'},
    {name: 'accounts-base', 'slug': 'users'},
    {name: 'accounts-twitter', 'slug': 'users'},
    {name: 'accounts-facebook', 'slug': 'users'},
    {name: 'accounts-google', 'slug': 'users'},
    {name: 'accounts-ui-unstyled', 'slug': 'users'},
    {name: 'ian:accounts-ui-bootstrap-3', 'slug': 'users'},

    // ------------------------------ Routing ------------------------------ //
    {name: 'iron:router', 'slug': 'routing'},
    {name: 'zimme:iron-router-active', 'slug': 'routing'},
    {name: 'meteorhacks:flow-router', 'slug': 'routing'},
    {name: 'okgrow:iron-router-autoscroll', 'slug': 'routing'},
    {name: 'settinghead:auto-nprogress', 'slug': 'routing'},

    // ------------------------------ i81n ------------------------------ //
    {name: 'tap:i18n', 'slug': 'i18n'},
    {name: 'tap:i18n-ui', 'slug': 'i18n'},
    {name: 'softwarerero:accounts-t9n', 'slug': 'i18n'},



    // ------------------------------ Security ------------------------------ //
    {name: 'ongoworks:security', 'slug': 'security'},
    {name: 'browser-policy', 'slug': 'security'},
    {name: 'meteorhacks:sikka', 'slug': 'security'},
];


//









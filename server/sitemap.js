sitemaps.add('/sitemap.xml', function () {
    var out = [];
    var packages = Packages.find({}, {sort: {createdAt: -1}}).fetch();
    _.each(packages, function (p) {
        out.push({
            page: 'package/' + p.slug,
            lastmod: p.updatedAt
        });
    });
    return out;

});
// ------------------------------ Security Methodes ------------------------------ //

Security.defineMethod("ifIsCurrentUser", {
    fetch: [],
    transform: null,
    deny: function (type, arg, userId, doc) {
        return userId !== doc.user;
    }
});


// ------------------------------ Categories ------------------------------ //

Categories.permit(['remove', 'insert', 'update']).ifHasRole('admin').apply();

//SubCategories
SubCategories.permit(['remove', 'insert', 'update']).ifHasRole('admin').apply();


// ------------------------------ Packages ------------------------------ //
Packages.permit(['remove', 'insert', 'update']).ifHasRole('admin').apply();


// ------------------------------ BigCounter ------------------------------ //
BigCounters.permit(['remove', 'insert', 'update']).ifHasRole('admin').apply();
Favorites.permit(['remove', 'insert', 'update']).ifHasRole('admin').apply();
// ------------------------------ Favorites ------------------------------ //

//Allow only document owner to insert or remove
Favorites.permit(['insert', 'remove', 'update']).ifIsCurrentUser().apply();



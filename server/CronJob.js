var everyDay = new Cron(function () {
    var packagesList = Packages.find();
    packagesList.forEach(function (row) {
        FlashMessages.insert({
            package: row.name,
            createdAt: new Date()
        });
     Meteor.call('updatePackage', row._id, row.name, row.category.id)
    });

    FlashMessages.remove({});

}, {
  hour: 24
});
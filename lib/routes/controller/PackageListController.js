PackageListController = RouteController.extend({
    template: 'packageList',
    slug: function () {

        if (this.params.sub_slug) {
            return this.params.sub_slug;
        }

        return this.params.slug;
    },

    waitOn: function () {
        Session.set('showingItems', globalSettings.packageLimit);
        return subManagerTest.subscribe('packages', this.slug(), globalSettings.packageQuery());
    },

    categoryInfo: function () {
        return Categories.findOne({slug: this.slug()});

    },
    packages: function () {
        return Packages.find({});
    },

    onAfterAction: function () {
        var category = this.categoryInfo();

        var currentPackageCount = category && category.packageCount;

        //is the current counter smaller than the limit or equal(20) then we can disable pagination
        Session.set('packageCount', category && category.packageCount);

        if (currentPackageCount < globalSettings.packageLimit) {
            Session.set('showingItems', category && category.packageCount)
        }


        SEO.set({
            title: category && category.name + ' for MeteorJS',
            meta: {
                'description': category && category.description
            },
            og: {
                'title': category && category.name + ' for MeteorJS',
                'description': category && category.description
            }
        });


    },

    data: function () {
        var self = this;
        return {
            getCategoryInfo: self.categoryInfo(),
            packages: self.packages()
        }
    }
});


Router.route('category/:slug/', {
    name: 'packages',
    controller: PackageListController
});

Router.route('category/:slug/:sub_slug', {
    name: 'subpackages',
    name: 'packageList',

    controller: PackageListController
});
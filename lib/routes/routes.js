Router.configure({
    layoutTemplate: 'main',
    loadingTemplate: 'loader',
    notFoundTemplate: 'notFound'
});



Router.route('/', {
    template: 'categories',
    name: 'test',
    waitOn: function () {

        return Meteor.subscribe('categories');
    },
//Sub available from the Footer oO
    data: function () {
        var categoryList = {
            categories: Categories.find({"packageCount": {$exists: true, $ne: 0}})
        };
        return categoryList;
    },

    onAfterAction: function () {
        SEO.set({
            title: 'Handpicked MeteorJS Packages',
            meta: {
                'description': 'Best packages for Meteor JS'
            },
            og: {
                title: 'Handpicked MeteorJS Packages',
                'description': 'Best packages for Meteor JS'
            }
        });


    }

});


Router.route('/package/:slug', {
    template: 'singlePackage',

    waitOn: function () {

        return Meteor.subscribe('singlePackage', this.params.slug);

    },


    data: function () {
        return {package: Packages.findOne({slug: this.params.slug})}
    },


    onAfterAction: function () {
        var singlePackage = this.data().package;
        SEO.set({
            title: singlePackage && singlePackage.name + ' for MeteorJS',
            meta: {
                'description': singlePackage && singlePackage.description
            },
            og: {
                'title': singlePackage && singlePackage.name + ' for MeteorJS',
                'description': singlePackage && singlePackage.description
            }
        });
    }

});


//User Routes
Router.route('/sign-in', function () {
    this.render('register');
});

Router.route('/sign-out', function () {
    Meteor.logout();
    Router.go('/');
});


Router.route('/favorites', {
    template: 'favorites',
    waitOn: function () {
  return Meteor.subscribe('favoritesPackages');
    },

   data: function () {
        var favorites;
        var FavPackages = []
        favorites = Favorites.find({user: Meteor.userId()}).fetch();
        _.each(favorites, function (fav) {
            if (Packages.findOne({_id: fav.packageId})) {
                return FavPackages.push(Packages.findOne({_id: fav.packageId}))

            }
        });

        return {favorites: FavPackages}
    }
});


//Search Route

Router.route('/search', {
    template: 'searchResults'
});


//Admin Routes
Router.route('/admin/add/package', {
    template: 'addPackage',
    waitOn: function () {
        return Meteor.subscribe('categories');
    }
});

Router.route('/admin/', {

    waitOn: function () {


        return Meteor.subscribe('categories');
    },

    data: function () {
        return {
            categories: Categories.find()
        }

    },

    template: 'mainAdmin'
});


// ------------------------------ Debug Routes ------------------------------ //

Router.route('/reset', function () {
    Meteor.call('removeCollections');
});
Categories = new Mongo.Collection('categories');
//Creating the Schema
Categories.attachSchema(new SimpleSchema({
        name: {
            type: String,
            label: 'Title',
            optional: false,
            unique: true
        },
        slug: {
            type: String,
            label: 'Title',
            optional: true,
            autoform: {
                omit: true
            }

        },
        description: {
            type: String,
            label: 'Description',
            optional: false,
            autoform: {
                rows: 5
            }
        },
        packageCount: {
            type: Number,
            optional: true,
            autoform: {
                omit: true
            }
           // autoValue:function(){ return 0 }

        }
    })
);





Favorites = new Mongo.Collection('favorites');

Favorites.attachSchema(new SimpleSchema({
    packageId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id
    },
    user: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
    },

    createdAt: {
        type: Date,
        autoValue: function () {
            return new Date();
        }
    }
}));




SubCategories = new Mongo.Collection('subcategories');

Schema.SubCategories = new SimpleSchema({
    

    parentId: {
        type: String,
        label: 'Category',
        optional: false,
        unique: true
    },
//
    name: {
        type: String,
        label: 'Title',
        optional: false,
        unique: true
    },
    description: {
        type: String,
        label: 'Description',
        optional: false,
    },
    slug: {
        type: String,
        label: 'Title',
        optional: true,
        autoform: {
            omit: true
        }
    },

    packageCount: {
        type: Number,
        optional: true,
        autoform: {
            omit: true
        }
        }
    });

UserCollections = new Meteor.Collection('usercollections');

UserCollections.attachSchema(new SimpleSchema({
    name: {
        type: String,
    },

    packageList: {
        type: String,
    }
}));
globalSettings = {
    packageLimit: 10,
    packageQuery: function () {
        //This is the Standard one for the first load of packages
        return {
            sort: {
                createdAt: -1
            },
            limit: globalSettings.packageLimit, skip: 0
        }
    }
};




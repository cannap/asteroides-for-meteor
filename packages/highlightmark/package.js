Package.describe({
    name: 'cannap:highlightmark',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: 'fork from https://github.com/stubailo/meteor-highlight.js',
    // URL to the Git repository containing the source code for this package.
    git: '',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Npm.depends({
    "html-entities": "1.1.1"
});

Package.onUse(function (api) {
    api.versionsFrom('1.1.0.2');
    api.addFiles('js/highlight.js',['server','client']);
    api.addFiles('css/default.css');

 //   api.addFiles('highlightmark.js');
 //   api.use('markdown@1.0.4',['client','server'], {weak: true});
    api.export('hljs')
});


/*
 Package.onTest(function(api) {
 api.use('tinytest');
 api.use('cannap:highlightmark');

 });
 */
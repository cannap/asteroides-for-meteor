AutoForm.addHooks(['packageForm'], {
    onSuccess: function (operation, result) {
        if (result) {
            Session.set('packageSubmitErrorMsg', false)
        }
    },
    onError: function (operation, error) {
        Session.set('packageSubmitErrorMsg', error.error)
    }

});
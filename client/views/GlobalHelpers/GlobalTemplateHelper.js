Template.registerHelper('isLoggedIn', function () {
    return !!Meteor.user();
});


Template.registerHelper('niceDate', function (timestamp) {
    return moment(timestamp).format('MMM\'DD YY');
});


// ------------------------------ Email ------------------------------ //
Template.registerHelper('userEmail', function () {
    //Todo: we need a username later..
    var user = Meteor.user();
    if (user && user.emails)
        return user.emails[0].address;

});
// ------------------------------ Check user is loggedinl ------------------------------ //
Template.registerHelper('isLoggedin', function () {
    if (Meteor.user()) {
        return true
    } else {
        return false
    }
});


// ------------------------------ Global Package Counter ------------------------------ //

Template.registerHelper('packageCounter', function () {
    var packageCount;
    packageCount = BigCounters.findOne({name: 'packageCounter'});
    return packageCount && packageCount.count;
});

// ------------------------------ Copy to Clipboard ------------------------------ //
Template.registerHelper('prepareForCopy', function (html) {
    return 'meteor add ' + html.replace(/<\/?[^>]+(>|$)/g, "");
});



Template.registerHelper('selectCategory', function (html) {
    return Categories.find().map(function (c) {
        return {label: c.name, value: c._id};
    });
});



// ------------------------------ DEBUG  ------------------------------ //
Template.registerHelper("debug", function(optionalValue) {
    console.log("Current Context");
    console.log("====================");
    console.log(this);

    if (optionalValue) {
        console.log("Value");
        console.log("====================");
        console.log(optionalValue);
    }
});
helpers = {


    // ------------------------------ Copty To Clipboard ------------------------------ //
    copyToClipBoard: function (elm) {
        var copy_sel = elm;
        copy_sel.on('click', function (e) {
            e.preventDefault();
        });
        copy_sel.clipboard({
            path: '/assets/swf/jquery.clipboard.swf',
            copy: function () {
                var this_sel = $(this);
                // Hide "Copy" and show "Copied, copy again?" message in link
                this_sel.find('.code-copy-first').hide();
                this_sel.find('.code-copy-done').show();

                // Return text in closest element (useful when you have multiple boxes that can be copied)
                return $(this).data('zclip-text');
            },


            afterCopy: function () {
                toastr.success('copied');
            }
        });
    },
};
Template.mainAdmin.events({

    'click .addCategory': function () {

    },

    'click .addSubCategory': function () {
        var self = this;
        swal({
            title: this.name,
            text: "Add Subcategory",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Write something"
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to write something!");
                return false
            }


            SubCategories.insert({name: inputValue, parent: self._id});
            swal.close();

        });
    },
    'click .removeSubCategory': function () {
var self = this;
        swal({
                title: "Deleting: " + this.name,
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    SubCategories.remove({_id: self._id})

                }

            });



    }
});


Template.mainAdmin.helpers({
    'hasSubCategory': function (parentId) {
        var subCategories = SubCategories.find({parent: parentId});
        if (subCategories) {
            Session.set('subCategories', parentId);
            return true;
        } else {
            return false;
        }
    },
    
    'subCategories': function (parentId) {
        return SubCategories.find({parent: parentId});
    }
});
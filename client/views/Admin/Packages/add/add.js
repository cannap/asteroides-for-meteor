Template.addPackage.helpers({
    packageFormSchema: function () {
        return Schema.Packages;
    },

    packageSubmitError: function () {
        var isError = Session.get('packageSubmitErrorMsg')
        return isError;
    }
});



// ------------------------------ Destroyed ------------------------------ //

Template.addPackage.onDestroyed(function () {
    Session.set('packageSubmitErrorMsg', false);
});
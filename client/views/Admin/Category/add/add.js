


// ------------------------------ Events ------------------------------ //
Template.addPackage.events = {
    'submit .addPackage': function (event) {
        event.preventDefault();
        var packageName = event.target.packageName.value;
        Meteor.call('getPackageByName', packageName)
    }
};

// ------------------------------ Rendered ------------------------------ //


Template.addPackage.rendered = function () {
    this.$('.dropdown')
        .dropdown({
            transition: 'slide down'
        });
};


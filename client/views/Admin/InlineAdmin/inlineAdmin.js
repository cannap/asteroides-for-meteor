Template.inlineAdminActions.events({
    'click .deletePackage': function () {
        var self = this;

        swal({
                title: "Deleting: " + this.name,
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    Meteor.call('removePackage', self._id, function (result) {
                        //   Router.go(document.referrer);
                    });
                }

            });
    },

// ------------------------------ Update current Package ------------------------------ //
    //Todo: later add a cronjob for this
    'click .updatePackage': function () {
        var self = this;
        Meteor.call('updatePackage', this._id, this.name, this.category.id, function (err, result) {
            if (!err) {
                toastr.success(self.name + ' updated!');
            } else {
                toastr.error(err.message);
            }
        });
    }
});
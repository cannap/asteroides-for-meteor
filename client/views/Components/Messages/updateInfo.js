Template.updateInfo.onCreated(function () {
    Meteor.subscribe('flashMessages');
});


Template.updateInfo.helpers({

    'isUpdating': function () {

        return FlashMessages.findOne({});
    },

    'lastUpdatePackage': function () {


        return FlashMessages.findOne({}, { sort: { createdAt: -1}});

    }
});
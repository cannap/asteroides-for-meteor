Template.userButtons.events({
    'click .fav': function () {
        //First we Check if the user has this Doc already favorited then remove else insert
        var favorite;

        favorite = Favorites.findOne({
            packageId: this._id,
            user: Meteor.userId()
        });

        if (favorite) {
            return Favorites.remove({_id: favorite._id});
        }

        return Favorites.insert({
            packageId: this._id,
            user: Meteor.userId(),
            createdAt: new Date()
        });
    }
});

Template.userButtons.helpers({
    isFavoritedByUser: function () {

        var favorite;
        favorite = Favorites.findOne({
            packageId: this._id,
            user: Meteor.userId()
        });

        return favorite;
    }
});



// ------------------------------ Search Settings ------------------------------ //
var options = {
    keepHistory: 1000 * 60 * 5,
    localSearch: true
};

var fields = ['name', 'description'];
PackageSearch = new SearchSource('packages', fields, options);

// ------------------------------ Events ------------------------------ //
Template.searchBar.events({
    'click .search_trigger': function (e) {
        e.preventDefault();
        var input = $('#search_string');
        if ($(e.target).hasClass('close')) {
            input.removeClass('active_search').val('');
            $(e.target).removeClass('ion-close-round close').addClass('ion-search');
            return;
        }

        input.show().addClass('active_search').focus();
        $(e.target).removeClass('ion-search').addClass('ion-close-round close');
    },

    'keyup #search_string': _.throttle(function (e) {
        var currentRoute = Router.current().route.getName();
        if (currentRoute == 'search') {
            var text = $(e.target).val().trim();
            PackageSearch.search(text);
        } else {
            Router.go('/search');
        }
    }, 300),
    'click .close': function () {
        var currentRoute = Router.current().route.getName();
        if (currentRoute != 'search') {
            return true;
        }
        history.back();
    }
});
// ------------------------------ Helpers ------------------------------ //
Template.searchResults.helpers({
    getPackages: function () {
        return PackageSearch.getData({
            transform: function (matchText, regExp) {
                return matchText.replace(regExp, "<strong>$&</strong>")
            }
        });
    },

    isLoading: function () {
        return PackageSearch.getStatus().loading;
    }
});

Template.searchResults.onRendered(function () {
    helpers.copyToClipBoard($('.fast_copy'));
});
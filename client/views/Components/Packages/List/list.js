subManagerTest = new SubsManager();

var slug;
Template.packageList.events({

    // ------------------------------ Load more ------------------------------ //
    'click .load_more': function () {
        var slug = Router.current().params.slug;
        Session.set('slug', slug);

        var skipLimit = parseInt(Session.get('showingItems'));
        var limit = parseInt(globalSettings.packageLimit)

        subManagerTest.subscribe('packages', Session.get('slug'), {sort:{createdAt: -1}, limit: limit, skip: skipLimit}, function () {

            Session.set('showingItems', $('.item-inner').length);


        });
    }
});


// ------------------------------ Rendered ------------------------------ //


    Template.packageList.rendered = function () {
        var container = this.find('.each-animated');
        this.find(container)._uihooks = {
            insertElement: function (node, next) {
                $(node).addClass('hidden').insertBefore(next);

                //Attache CopyToClipBoard on new Elements
                helpers.copyToClipBoard($(node).find('.fast_copy'));

                return Tracker.afterFlush(function () {
                    return $(node).removeClass('hidden');
                });
            },

            removeElement: function (node) {
                var finishEvent;
                finishEvent = 'webkitTransitionEnd oTransitionEnd transitionEnd msTransitionEnd transitionend';
                $(node).addClass('hidden');
                return $(node).on(finishEvent, function () {
                    return $(node).remove();
                });
            }
        };

        // ------------------------------ Copy to Clipboard ------------------------------ //
        helpers.copyToClipBoard($('.fast_copy'));

    };


// ------------------------------ Helpers for Packagelist ------------------------------ //

    Template.packageList.helpers({
        showingItems: function () {
            return Session.get('showingItems');
        }

    });


// ------------------------------ Destroy  packageList ------------------------------ //

    Template.packageList.onDestroyed(function () {
        subManagerTest.clear();
    });

// ------------------------------ Load more ------------------------------ //

    Template.loadMore.helpers({
        loadMore: function () {
            if (Session.get('packageCount') == Session.get('showingItems')) {
                return {
                    disable: 'disabled',
                    buttonText: 'There is nothing more to load'
                }
            } else {
                //Todo: how much left
                return {
                    disable: '',
                    buttonText: 'Load more'
                }
            }
        }
    });
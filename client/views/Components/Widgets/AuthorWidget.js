Template.authorWidget.helpers({
    getPackageByAuthor: function (authorName) {
        return Packages.find({'details.author': authorName})
    }
});
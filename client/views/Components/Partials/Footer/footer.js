// ------------------------------ Created ------------------------------ //
Template.footer.onCreated(function () {
    this.subscribe('favorites');
    this.subscribe('footerPackageCount');
    this.subscribe('categories')
});

// ------------------------------ Rendered ------------------------------ //
Template.footer.rendered = function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('.back_top').fadeIn();
        } else {
            $('.back_top').fadeOut();
        }
    });
};


Template.footer.events({
    'click .back_top': function () {
        console.log('test');


        $('body, html').animate({scrollTop: 0}, 150);
    }
});

// ------------------------------ Helpers ------------------------------ //
Template.footer.helpers({
    footerCategories: function () {
        return Categories.find({"packageCount": {$exists: true, $ne: 0}});
    }
});